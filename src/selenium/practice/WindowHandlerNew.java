package selenium.practice;

import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class WindowHandlerNew {

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver", "D:\\\\Automation testing\\\\chromedriver\\\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();

		driver.get("http://demo.guru99.com/popup.php");
		driver.manage().window().maximize();
		String parent = driver.getWindowHandle();
		driver.findElement(By.xpath("//h2[contains(text(),'Guru99 Bank')]//following::a")).click();

		Set<String> allwindow = driver.getWindowHandles();
		Iterator<String> I1 = allwindow.iterator();

		while (I1.hasNext()) {

			String child = I1.next();

			if (!parent.equals(child)) {

				driver.switchTo().window(child);

				System.out.println(driver.switchTo().window(child).getTitle());

			}

		}
		driver.switchTo().window(parent);

	}

}
