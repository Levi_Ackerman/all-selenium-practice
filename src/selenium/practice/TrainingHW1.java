package selenium.practice;

import java.util.List;
import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class TrainingHW1 {

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver", "E:\\Chrome Driver\\chromedriver.exe");
//		WebDriver driver = new ChromeDriver();
		
		
		//Headless Browser
		ChromeOptions options = new ChromeOptions();
        options.addArguments("--headless");     
        options.addArguments("--disable-gpu");
        options.addArguments("--window-size=1400,800");  
        WebDriver driver = new ChromeDriver(options);

		driver.get("https://www.indiatoday.in/news.html");
		driver.manage().window().maximize();
		System.out.println("///////////////////////  Sports news ////////////////////////////////");
		List<WebElement> sports = new ArrayList<WebElement>();
		sports = driver.findElements(By.xpath("//div[@id='card_1206550_itg-news-section-4']//following-sibling::p//child::a"));
		for(int i=0;i<sports.size()-3;i++)  
        {  
         System.out.println(sports.get(i).getText());     
        }  
		System.out.println("\n");
		System.out.println("/////////////////////   Movies news  ////////////////////////////////");
		
		List<WebElement> movies = new ArrayList<WebElement>();
		movies = driver.findElements(By.xpath("//div[@id='card_1206533_itg-news-section-2']//following-sibling::p//child::a"));
		for (int j = 0; j < movies.size()-3; j++) {
			System.out.println(movies.get(j).getText());
		}

		// div[@class='widget-wrapper
		// section_wise_order-news-section']//a[text()='Sports']//following::p//child::a
		//h3[@class='frist-heading heading-1854361 story ']//following::p//child::a
		//div[@id='card_1206550_itg-news-section-4']//following-sibling::p//child::a
//		WebElement sportsNews = driver.findElement(By.xpath(
//				"//div[@id='card_1206550_itg-news-section-4']//following-sibling::p//child::a"));
		//card_1206533_itg-news-section-2
//		
//		int news = sportsNews.size();
//		System.out.println(news);
//		WebElement news1 ;
//		
//		for(int i=1;i<news-3;i++)
//			String news3 = sportsNews.findElements(By.xpath("//div[@id='card_1206550_itg-news-section-4']//following-sibling::p//child::a")).get(i);
		}
	}


