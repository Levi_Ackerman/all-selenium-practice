package selenium.practice;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class GmailTesting {

	public static void main(String[] args) throws InterruptedException {

		// Setting the webdriver.chrome.driver property to its executable's location
//		System.setProperty("webdriver.chrome.driver", "D:\\Automation testing\\chromedriver\\chromedriver.exe");

		// Instantiating driver object
//		WebDriver driver = new ChromeDriver();
		
		System.setProperty("webdriver.gecko.driver", "E:\\geckodriver\\geckodriver.exe");
		// Instantiating driver object
		WebDriver driver = new FirefoxDriver();
		

		// Using get() method to open a web page

		driver.navigate().to(
				"https://accounts.google.com/signin/v2/identifier?continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&service=mail&sacu=1&rip=1&flowName=GlifWebSignIn&flowEntry=ServiceLogin");
		driver.manage().window().maximize();

		driver.findElement(By.xpath("//span[text()='Continue to Gmail']//following::input[1]"))
				.sendKeys("automationt71@gmail.com");
		driver.findElement(By.xpath("//div[@id='identifierNext']//following::span[text()='Next']")).click();

		Thread.sleep(2000);

		driver.findElement(By.xpath("//div[contains(text(),'Enter')]//preceding::input[1][@type='password']"))
				.sendKeys("Automation4868");

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		driver.findElement(By.xpath("//div[@jsname='QkNstf']//preceding::span[text()='Next']")).click();

		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		
		 driver.findElement(By.xpath("//div[contains(@style,'user')]//child::div[@role='button']")).click();
		
		 driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		
		 driver.findElement(By.xpath("//textarea[contains(@name,'cc')]//ancestor::div[3]//child::textarea[@name='to']"))
		 .sendKeys("bhowmickpinaki123@gmail.com");
		 
		 driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		 
		 driver.findElement(By.xpath("//div[@role='menu']//preceding::div[contains(@id,':')]//child::input[contains(@placeholder,'Sub')]"))
		 .sendKeys("Test mail");
		
		 driver.findElement(By.xpath("//div[contains(@style,'display')]//child::div[contains(@role,'text')]"))
		 .sendKeys("Hi there, this is an testing mail for automation");
		
		 driver.findElement(By.xpath("//div[contains(@id,':')]//preceding-sibling::div[contains(@aria-label,'Send')]")).click();
		 
		 driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		 
		 driver.findElement(By.xpath("//div[contains(@class,'aio')]//child::a[contains(text(),'Sent')]")).click();
		 
		 driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		 WebElement text = driver.findElement(By.xpath("//span[contains(@id,':')]//child::span[text()='Test mail']"));
		 
		 String str = text.getText();
		 if(str.contentEquals("Test mail")) {
			 System.out.println("Email sent");
		 } else {
			 System.out.println("Email not sent");
		 }

//		Thread.sleep(8000);
//
//		driver.findElement(By.xpath("//tr[@class='zA yO']//child::td[@class='xY a4W']")).click();
//		driver.findElement(By.xpath("//span[@class='ams bkG']//preceding-sibling::span")).click();
//
//		driver.findElement(By.xpath("//div[@class='Ar Au']//child::div[@class='Am aO9 Al editable LW-avf tS-tW']"))
//				.sendKeys("Welcome");

	}

}
