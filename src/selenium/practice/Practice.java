package selenium.practice;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class Practice {
	
	public static void main(String[] args) throws InterruptedException {
		//Setting the webdriver.chrome.driver property to its executable's location
	    System.setProperty("webdriver.chrome.driver", "D:\\chromedriver\\chromedriver.exe");

	    //Instantiating driver object
	    WebDriver driver = new ChromeDriver();
	    
	    //Using get() method to open a web page

	    driver.get("https://www.flipkart.com/");
	    driver.manage().window().maximize();
	    
	    
	    //fill user name
	    
	    driver.findElement(By.xpath("(//input[@type = 'text'])[2]")).sendKeys("7980451631");
	    
	    //fill password
	    
	    driver.findElement(By.xpath("//input[@type = 'password']")).sendKeys("flipkart4868");
	    
	    //for submit
	    
	    driver.findElement(By.xpath("(//button[@type='submit'])[2]")).click();
	    
	    Thread.sleep(1000);
	    
	    //identify the element 
	    WebElement l = driver.findElement(By.xpath("//div[text()='Pinaki ']"));
	    
	    //action class with move to element
	    Actions a = new Actions(driver);
	    
	    a.moveToElement(l).perform();
	}

}
