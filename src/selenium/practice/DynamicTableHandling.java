package selenium.practice;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DynamicTableHandling {

	public static void main(String[] args) {
		
		
		System.setProperty("webdriver.chrome.driver", "D:\\Automation testing\\chromedriver\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		driver.get("https://www.freecrm.com/");
		driver.manage().window().maximize();
		
		//click login
		driver.findElement(By.xpath("//span[text()='Log In']")).click();
		
		//driver.manage().timeouts().implicitlyWait(5000, TimeUnit.MILLISECONDS);
		
		WebDriverWait wait=new WebDriverWait(driver, 20);
		WebElement element;
		element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@type='text' and @name='email']")));
		
		element.sendKeys("bhowmickpinaki123@gmail.com");
		
		//type email
//		driver.findElement(By.xpath("//input[@type='text' and @name='email']")).sendKeys("bhowmickpinaki123@gmail.com");
		
		//type password
		driver.findElement(By.xpath("//input[@type='password' and @name='password']")).sendKeys("Test@4868");
		
		//click login
		driver.findElement(By.xpath("//div[text()='Login']")).click();
		
		WebElement element1;
		element1 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//i[@class='users icon']//following::span[text()='Contacts']")));
		element1.click();
		
		//click on contacts
		//driver.findElement(By.xpath("//i[@class='users icon']//following::span[text()='Contacts']")).click();
		
		driver.findElement(By.xpath("//div[@id='dashboard-toolbar']")).click();
		//click the check box
		WebElement element2;
		
		WebDriverWait wait1=new WebDriverWait(driver, 40);
		element2 = wait1.until(ExpectedConditions.elementToBeClickable((By.xpath("//a[contains(text(),'abc  d')]//parent::td//preceding-sibling::td//input[@name='id']"))));
		element2.click();
//		driver.findElement(By.xpath("//a[contains(text(),'abc  d')]//parent::td//preceding-sibling::td//input[@name='id']")).click();
		
		driver.findElement(By.xpath("//a[contains(text(),'Pinaki')]//parent::td//preceding-sibling::td//input[@name='id']")).click();
	}

}
