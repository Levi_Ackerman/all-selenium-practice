package selenium.practice;

import java.awt.AWTException;
import java.awt.Robot;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Practice2 {

	public static void main(String[] args) throws AWTException, InterruptedException {
		// TODO Auto-generated method stub
		
		System.setProperty("webdriver.chrome.driver", "D:\\chromedriver\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
//		driver.get("https://www.onlinestores.com/home-furniture.html");
		driver.get("https://freecrm.com/");
		driver.manage().window().maximize();
		
		Actions act = new Actions(driver);
		WebElement ele = driver.findElement(By.xpath("//a[contains(text(),'Free CRM')]"));
		act.moveToElement(ele).clickAndHold().build().perform();
		driver.findElement(By.xpath("//ul[@class='rd-navbar-dropdown rd-navbar-open-right']//li//a[@href='ps.html'][contains(text(),'Professional Services CRM')]")).click();
		
		
		
		Robot rbt = new Robot();
		rbt.mouseWheel(30);
		driver.findElement(By.xpath("//a[contains(text(),'Privacy Policy')]")).click();
		
		
//		WebElement ele = driver.findElement(By.xpath("//img[@src='https://cdn11.bigcommerce.com/s-6e1n67clqw/images/stencil/90w/n/api0npqkp__34502.original.png']"));
//		act.contextClick(ele).build().perform();
//		act.doubleClick(ele).build().perform();
		
//		driver.findElement(By.xpath("//a[contains(text(),'Bedroom Furniture')]")).click();
//		driver.manage().timeouts().implicitlyWait(100, TimeUnit.MILLISECONDS);
//		String title1 = driver.getTitle();
//		System.out.println(title1);
//		driver.navigate().back();
//		driver.manage().timeouts().implicitlyWait(100, TimeUnit.MILLISECONDS);
//		String title2 = driver.getTitle();
//		System.out.println(title2);
//		driver.navigate().back();
		
		// wait
		Thread.sleep(1000);
		
		//implicit wait
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		//explicit wait 
		WebDriverWait wait = new WebDriverWait(driver, 20);
		//this can be put in an WebElement and use that to perform any other action
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[contains(text(),'Bedroom Furniture')]")));
		
		

		
		
	}

}
