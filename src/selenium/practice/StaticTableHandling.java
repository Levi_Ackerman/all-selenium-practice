package selenium.practice;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class StaticTableHandling {

	public static void main(String[] args) {
		
		System.setProperty("webdriver.chrome.driver", "D:\\Automation testing\\chromedriver\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		driver.get("https://www.w3schools.com/html/html_tables.asp");
		driver.manage().window().maximize();
		
		String First = "//table[@id='customers']/tbody/tr[";
		String Second = "]/td[";
		String Third = "]";
		System.out.println(driver.findElement(By.xpath(First+"3"+Second+"3"+Third)).getText());

	}

}
