package selenium.practice;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class TableExercise {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int sum = 0;
		System.setProperty("webdriver.chrome.driver", "D:\\Automation testing\\chromedriver\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.cricbuzz.com/live-cricket-scorecard/36687/dgd-vs-slst-15th-match-tamil-nadu-premier-league-2021");
		WebElement table  = driver.findElement(By.cssSelector("div[class='cb-col cb-col-100 cb-ltst-wgt-hdr']"));
		int rowcount = table.findElements(By.cssSelector("div[class='cb-col cb-col-100 cb-scrd-itms']")).size();
		int count = table.findElements(By.cssSelector("div[class='cb-col cb-col-100 cb-scrd-itms'] div:nth-child(3)")).size();
		
		for (int i = 0; i < count-2; i++) 
		{
			String var = table.findElements(By.cssSelector("div[class='cb-col cb-col-100 cb-scrd-itms'] div:nth-child(3)")).get(i).getText();
			int varint = Integer.parseInt(var);
			sum = sum + varint;
		}
		
		String extr = driver.findElement(By.xpath("//div[text()= 'Extras']/following-sibling::div")).getText();
		int extrint = Integer.parseInt(extr);
		int total = sum + extrint;
		System.out.println(total);
		
		String actualtotal = driver.findElement(By.xpath("//div[text()= 'Total']/following-sibling::div")).getText();
		int actualtotalint = Integer.parseInt(actualtotal);
		
		if (actualtotalint==total)
		{
			System.out.println("Correct");
		} else 
		{
			System.out.println("Incorrect");
		}
		
	}

}
