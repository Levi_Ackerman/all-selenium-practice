package selenium.practice;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Practice1 {

	public static void main(String[] args) throws InterruptedException {

		// Setting the webdriver.chrome.driver property to its executable's location
		System.setProperty("webdriver.chrome.driver", "D:\\chromedriver\\chromedriver.exe");

		// Instantiating driver object
		WebDriver driver = new ChromeDriver();

		// Using get() method to open a web page

		driver.get("https://www.worldometers.info/world-population/");
		driver.manage().window().maximize();

		// wait for 20 sec.

		// Thread.sleep(20000);

		// driver.findElement(By.xpath("//div[text()='Births today ']"));

		// List<WebElement> m =
		// driver.findElements(By.xpath("//div[@class='maincounter-number']//span[@class='rts-counter']"));
		// iterate over list
		// for(int i = 0; i< m.size(); i++) {
		// //obtain text
		//
		// Thread.sleep(20000);
		// String s = m.get(i).getText();
		// System.out.println("Text is: " + s);
		// }
		
		int counter = 0;

		while (counter != 20) {

			List<WebElement> worldpopulation = driver
					.findElements(By.xpath("//div[@class='maincounter-number']//span[@class='rts-counter']"));

			// Thread.sleep(20000);
			// String s = m.get(m.size()).getText();
			for (WebElement w : worldpopulation) {

				System.out.println(w.getText());
			}
			Thread.sleep(1000);
			counter++;
//			driver.quit();
		}

	}

}
