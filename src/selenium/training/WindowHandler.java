package selenium.training;

//import java.util.ArrayList;
//import java.util.Iterator;
import java.util.Set;
//import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class WindowHandler {

	public static void main(String[] args) {
		
		System.setProperty("webdriver.chrome.driver", "D:\\\\Automation testing\\\\chromedriver\\\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		driver.get("https://demoqa.com/browser-windows");
		
		
		driver.findElement(By.xpath("//button[contains(text(),'Tab')]")).click();
		
		
		driver.findElement(By.xpath("//button[contains(text(),'Tab')]")).click();
		
		
		driver.findElement(By.xpath("//button[contains(text(),'Tab')]")).click();
		
		String parentwindow = driver.getWindowHandle();
		System.out.println(" Parent window : " + parentwindow);
		Set<String> allwindows = driver.getWindowHandles();
		
//		//Getting all the sets in terms of list 
//		ArrayList<String> tabs = new ArrayList<>(allwindows);
//		
//		//switching tab using object of list 
//		
//		//switch to first tab
//		driver.switchTo().window(tabs.get(0));
//		
//		//switch to second tab
//		driver.switchTo().window(tabs.get(1));
		
		System.out.println("Child Windows: " + allwindows);
		
		int NumberOfWindows = allwindows.size();
		System.out.println("Number of windows: " + NumberOfWindows);
		
		//Running enhance for loop 
		//It will take first window value and store it in child (for first iterate)
		//child variable store the same value of parent because it iterate both the values
		//for 2nd iteration child will take value of 2nd window 
		
		for(String child : allwindows)
		{
			//it will check the value child same or different from parent 
			//for first case it will take false because parent and child value same for first iteration
			//for 2nd iteration if case will be true 
			if(!parentwindow.equalsIgnoreCase(child))
			{
				//when if statement true it will switch to 2nd window 
				driver.switchTo().window(child);
				
				
			}
			
//			driver.switchTo().defaultContent();
//			driver.switchTo().window(parentwindow);
		}
		
//		driver.switchTo().parentFrame();
		
//		driver.switchTo().defaultContent();
		
//		driver.manage().timeouts().implicitlyWait(10000, TimeUnit.MILLISECONDS);
		//it switch to parent window 
		driver.switchTo().window(parentwindow);
		
		
		
		
	}

}
