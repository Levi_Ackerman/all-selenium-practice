package selenium.training;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class TableDataprint1 {

	public static void main(String[] args) {
		
		System.setProperty("webdriver.chrome.driver", "D:\\Automation testing\\chromedriver\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		driver.get("http://demo.guru99.com/test/web-table-element.php");
		driver.manage().window().maximize();
		
//		driver.findElement(By.xpath("//div[contains(@class,'div_topgainerhd')]//following::a[contains(text(),'Group B')]")).click();
		
		List<WebElement> rows = driver.findElements(By.xpath("//div[contains(@style,'clear:both')]//following::table/tbody/tr"));
		System.out.println("No. of rows : " +rows.size());
		
		List<WebElement> coloumns = driver.findElements(By.xpath("//div[contains(@style,'clear:both')]//following::table/tbody/tr[1]/td"));
		System.out.println("No. of coloumns : "+coloumns.size());
		
		String Table_First = "//div[contains(@style,'clear:both')]//following::table/tbody/tr[";
		String Table_Second = "]/td[";
		String Table_Third = "]";
		
		WebElement rowdata = driver.findElement(By.xpath(Table_First+"4"+Table_Third));
		System.out.println("Data of particular row : " +rowdata.getText());
		
		WebElement OneData = driver.findElement(By.xpath(Table_First+"4"+Table_Second+"4"+Table_Third));
		System.out.println("Data of partcular coulmn of particular row : " +OneData.getText());
		
		for (int i = 1; i <= rows.size(); i++) {
			System.out.println("Deatils of row " + i + " : ");
			System.out.println("----------------------------");
			
			for (int j = 1; j <= coloumns.size(); j++) {
				
				WebElement element = driver.findElement(By.xpath(Table_First+i+Table_Second+j+Table_Third));
				
				System.out.println(element.getText());
				
			}
		}
		
	}

}
