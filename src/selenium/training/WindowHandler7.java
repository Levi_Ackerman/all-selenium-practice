package selenium.training;

import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class WindowHandler7 {

	public static void main(String[] args) {
		
		System.setProperty("webdriver.chrome.driver", "D:\\\\Automation testing\\\\chromedriver\\\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		driver.get("https://www.naukri.com/");
		String Parent = driver.getWindowHandle();
		System.out.println("parent window: " + Parent);
		System.out.println(driver.getTitle());
		
		driver.get("https://www.google.com/");
		String child1 = driver.getWindowHandle();
		System.out.println(driver.getTitle());
		
		driver.get("https://www.bing.com/");
		String child2 = driver.getWindowHandle();
		System.out.println(driver.getTitle());
		
		Set<String> allwindows = driver.getWindowHandles();
		System.out.println("All windows: " + allwindows);
		System.out.println(driver.getTitle());
		
		Iterator<String> it = allwindows.iterator();
		String child = "";
		
		while (it.hasNext()) {
			child = it.next();
			if(!Parent.equalsIgnoreCase(child1))
			{
				driver.switchTo().window(child1);
				System.out.println(driver.getTitle());
				driver.findElement(By.xpath("//input[@type = 'search' and @name = 'q']")).sendKeys("java");
				
			} else if (!Parent.equalsIgnoreCase(child2)) {
				driver.switchTo().window(child2);
				System.out.println(driver.getTitle());
				driver.findElement(By.xpath("//input[@type='text']")).sendKeys("kolkata");
			}else {
				driver.switchTo().window(Parent);
				System.out.println(driver.getTitle());
			}
		
			driver.switchTo().window(Parent);
		}
		
	}

}
