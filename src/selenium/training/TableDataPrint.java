package selenium.training;

import java.io.File;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import com.google.common.io.Files;

public class TableDataPrint {
	public String getScreenhot(WebDriver driver, String screenshotName) throws Exception {
		String dateName = "today";
		TakesScreenshot ts = (TakesScreenshot) driver;
		File source = ts.getScreenshotAs(OutputType.FILE);
		// after execution, you could see a folder "FailedTestsScreenshots" under src
		// folder
		String destination = "D:\\" + screenshotName + dateName
				+ ".png";
		File finalDestination = new File(destination);
		Files.copy(source, finalDestination);
		return destination;
	}

	public static void main(String[] args) throws Exception {
		
		
		WebDriver driver;
		System.setProperty("webdriver.chrome.driver", "E:\\Chrome Driver\\chromedriver.exe");
		//Headless Browser
		ChromeOptions options = new ChromeOptions();
        options.addArguments("--headless");     
        options.addArguments("--disable-gpu");
        options.addArguments("--window-size=1400,800");  
        driver = new ChromeDriver(options);
		
        System.out.println("// Opening Website 1 //");
		driver.get("https://studyeasy.org/java/50-keywords-of-java/");
		
		driver.manage().window().maximize();
		TableDataPrint t = new TableDataPrint();
		t.getScreenhot(driver, "Website1");
		
		driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		
//		String First = "((//table//following::tr)[";
//		String Second = "]//following-sibling::td)[";
//		String Third = "]";
//		
//		String Data = driver.findElement(By.xpath(First+"3"+Second+"2"+Third)).getText();
//		System.out.println(Data);
		
		System.out.println("// Opening Website 2 //");
		driver.get("https://www.google.com/");
		t.getScreenhot(driver, "Website2");
		driver.manage().window().maximize();
		

	}

}
