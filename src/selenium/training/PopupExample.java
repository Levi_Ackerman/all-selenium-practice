package selenium.training;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class PopupExample {

	public static void main(String[] args) {
		
		// Setting the webdriver.chrome.driver property to its executable's location
				System.setProperty("webdriver.chrome.driver", "D:\\Automation testing\\chromedriver\\chromedriver.exe");

				// Instantiating driver object
				WebDriver driver = new ChromeDriver();
				
				driver.get("http://moviesprout.blogspot.com/");
				
				//opening the pop up window 
				driver.findElement(By.xpath("//a[contains(text(),' SEARCH')]")).click();
				
				//send key to the pop up window
				driver.findElement(By.xpath("//input[@name='q' and @value='Search the site']")).sendKeys("THE SHAWSHANK REDEMPTION");
				
				//search in pop up 
				driver.findElement(By.xpath("//input[@name='q' and @value='Search the site']//following::button[1]")).click();
				//close the pop up window
				//driver.findElement(By.xpath("//input[@name='q' and @value='Search the site']//following::button[2]")).click();

	}

}
