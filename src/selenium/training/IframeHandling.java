package selenium.training;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class IframeHandling {

	public static void main(String[] args) {
		
		System.setProperty("webdriver.chrome.driver", "D:\\Automation testing\\chromedriver\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		driver.get("http://demo.guru99.com/test/guru99home/");
		driver.manage().window().maximize();
		
		int count = driver.findElements(By.tagName("iframe")).size();
		System.out.println(count);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//iframe[@src='https:/www.youtube.com/embed/RbSlW8jZFe8']")));
		WebElement element;
		element = driver.findElement(By.tagName("iframe"));
		driver.switchTo().frame(element);
//		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='ytp-cued-thumbnail-overlay-image']//following-sibling::button")));
		driver.findElement(By.xpath("//div[contains(@class,'image')]//following-sibling::button[contains(@class,'button')]")).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@aria-label='Pause (k)']")));
		driver.findElement(By.xpath("//button[@aria-label='Pause (k)']")).click();
		
		driver.switchTo().frame("a077aa5e");
		
		driver.switchTo().defaultContent();
		driver.findElement(By.xpath("//li[@class='dropdown']//following-sibling::a[contains(text(),'Selenium')]")).click();
	}

}
