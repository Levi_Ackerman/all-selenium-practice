package selenium.training;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class AlertandPopup {

	public static void main(String[] args) {
		
		System.setProperty("webdriver.chrome.driver", "D:\\\\Automation testing\\\\chromedriver\\\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		driver.get("https://demoqa.com/alerts");
		driver.manage().window().maximize();
		
		driver.findElement(By.xpath("//button[@id= 'alertButton']")).click();
		Alert alert4 = driver.switchTo().alert();
		String c = alert4.getText();
		System.out.println(c);
		alert4.accept();
		
//		driver.findElement(By.xpath("//button[@id= 'timerAlertButton']")).click();
//		driver.manage().timeouts().implicitlyWait(6000, TimeUnit.MILLISECONDS);
//		driver.switchTo().alert().accept();
		
		
		driver.findElement(By.xpath("//button[@id= 'confirmButton']")).click();
		Alert alert2 = driver.switchTo().alert();
		String b = alert2.getText();
		System.out.println(b);
		alert2.dismiss();
		
		driver.findElement(By.xpath("//button[@id= 'promtButton']")).click();
		Alert alert3 = driver.switchTo().alert();
		alert3.sendKeys("Pinaki");
		
		String a = alert3.getText();
		alert3.accept();
		System.out.println(a);
		
		//actions class
//		Actions actions = new Actions(driver);
//		actions.contextClick(driver.findElement(By.xpath(""))).build().perform();
		

	}

}
