package selenium.training;

import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Windowhandle4 {

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver", "D:\\\\Automation testing\\\\chromedriver\\\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		driver.get("https://www.naukri.com/");
		String parent = driver.getWindowHandle();
		
		driver.findElement(By.xpath("//div[contains(text(),'Recruiters')]")).click();
		
		driver.findElement(By.xpath("//div[contains(text(),'Companies')]")).click();
		
		Set<String> windows = driver.getWindowHandles(); // handling the windows 
		Iterator<String> it =  windows.iterator(); //using iterator iterating through the windows
		
		while(it.hasNext()) {
			
			driver.switchTo().window(it.next());
			System.out.println(driver.getTitle());
			
		}
		driver.switchTo().window(parent);
	}

}
