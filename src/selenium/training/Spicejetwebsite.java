package selenium.training;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Spicejetwebsite {

	public static void main(String[] args){
		
		System.setProperty("webdriver.chrome.driver", "D:\\\\Automation testing\\\\chromedriver\\\\chromedriver.exe");
		
		WebDriver driver = new ChromeDriver();
		
		driver.get("https://www.spicejet.com/");
		driver.manage().window().maximize();
		
		//driver.findElement(By.xpath("//label[contains(text(),'Currency')]")).click();
		
		Select select =  new Select(driver.findElement(By.xpath("//label[contains(text(),'Currency')]//following::select[contains(@name,'ListCurrency')]")));
		
		select.selectByIndex(5);
		
//		select.selectByVisibleText("USD");
		
		driver.findElement(By.xpath("//label[text()=' Family & Friends']//preceding-sibling::input[@type='checkbox']")).click();
		
		driver.findElement(By.xpath("//label[contains(text(),'PASSENGER')]//following::div[1]")).click();
		
		Select adult = new Select(driver.findElement(By.xpath("(//label[contains(text(),'Adult')])[2]//following::select[1]")));
		adult.selectByIndex(1);
		Select child = new Select(driver.findElement(By.xpath("(//label[contains(text(),'Child')])[2]//following::select[1]")));
		child.selectByIndex(1);
//		Select infant = new Select(driver.findElement(By.xpath("(//label[contains(text(),'Infant')])[2]//following::select[1]")));
		
		driver.findElement(By.xpath("(//label[contains(text(),'FROM')])[1]//following::input[1]")).click();
		driver.findElement(By.xpath("//h3[contains(text(),'India')]//following::a[contains(text(),'CCU')]")).click();
		driver.manage().timeouts().implicitlyWait(2000, TimeUnit.MILLISECONDS);
		driver.findElement(By.xpath("(//h3[contains(text(),'India')])[2]//following::a[contains(text(),'KNU')]")).click();
		driver.findElement(By.xpath("(//table[@class='ui-datepicker-calendar'])[1]//following-sibling::td//a[contains(text(),'15')]")).click();
		
//		WebElement datepick = driver.findElement(By.xpath("//input[@name='ctl00$mainContent$view_date1']"));
//		
//		datepick.sendKeys("1507");
		
		
		driver.findElement(By.xpath("//label[contains(text(),'Round Trip')]//preceding-sibling::input[@type='radio']")).click();
		driver.findElement(By.xpath("(//table[@class='ui-datepicker-calendar'])[2]//following-sibling::td//a[contains(text(),'21')]")).click();
		
		
//		driver.findElement(By.xpath("//div[@id='Div6']//following::input[@type='submit' and @name='ctl00$mainContent$btn_FindFlights']")).click();
	}

}
