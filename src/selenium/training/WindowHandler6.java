package selenium.training;

import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class WindowHandler6 {

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver", "D:\\\\Automation testing\\\\chromedriver\\\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();

		driver.get("http://demo.guru99.com/popup.php");

		driver.findElement(By.xpath("//*[contains(@href,'popup.php')]")).click();
		// System.out.println(driver.getTitle());
		String MainWindow = driver.getWindowHandle();
		Set<String> s1 = driver.getWindowHandles();
		Iterator<String> i1 = s1.iterator();

		while (i1.hasNext()) {
			String ChildWindow = i1.next();

			if (!MainWindow.equalsIgnoreCase(ChildWindow)) // checking id with parent window
			{

				driver.switchTo().window(ChildWindow);
				driver.findElement(By.xpath("//input[@type='submit']//preceding::input")).sendKeys("djhcjhd@gmail.com");
				driver.findElement(By.xpath("//input[@type='text']//following::input")).click();
				driver.findElement(By.xpath("//a[text()='Click Here']")).click();
				driver.findElement(By.xpath("//h2[text()='Guru99 Bank']//following::a")).click();
				// System.out.println(driver.getTitle());

			}
		}
		driver.switchTo().window(MainWindow);
		// System.out.println(driver.getTitle());

	}
}
