package selenium.training;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
//import org.openqa.selenium.support.ui.Select;

public class LinktextTest {

	public static void main(String[] args) throws InterruptedException {

		// Setting the webdriver.chrome.driver property to its executable's location
		System.setProperty("webdriver.chrome.driver", "D:\\Automation testing\\chromedriver\\chromedriver.exe");

		// Instantiating driver object
		WebDriver driver = new ChromeDriver();

		// Using get() method to open a web page

		driver.get("https://www.flipkart.com/");
		driver.manage().window().maximize();

		// click to login

		// fill user name

		driver.findElement(By.xpath("(//input[@type = 'text'])[2]")).sendKeys("bhowmickpinaki123@gmail.com");

		// fill password

		driver.findElement(By.xpath("//input[@type = 'password']")).sendKeys("Flipkart4868");

		// for submit

		driver.findElement(By.xpath("(//button[@type='submit'])[2]")).click();

		// Thread.sleep() is a static Java method that suspends the code for a specific
		// amount of time.
		// It pauses the execution and helps us to know what has happened during the
		// pause
		// Handle Dynamic Elements:
		// Testing Third-Party Systems:

		Thread.sleep(1000);

		// identify the element
		WebElement l = driver.findElement(By.xpath("//div[text()='Pinaki ']"));

		// action class with move to element
		Actions a = new Actions(driver);

		a.moveToElement(l).perform();

		l.findElement(By.xpath("//div[text()='Logout']")).click();
		
		Thread.sleep(1000);
		// fill user name

		driver.findElement(By.xpath("(//input[@type = 'text'])[2]")).sendKeys("bhowmickpinaki123@gmail.com");

		// fill password

		driver.findElement(By.xpath("//input[@type = 'password']")).sendKeys("Flipkart4868");

		// for submit

		driver.findElement(By.xpath("(//button[@type='submit'])[2]")).click();
		
		

		// a.moveToElement(l).clickAndHold();

		// a.moveToElement(By.xpath("//div[text()='Logout']")).clickAndHold();

		// driver.findElement(By.xpath("//div[text() = 'My Account']")).click();

		// driver.findElement(By.xpath("//div[text()='Logout']")).click();

		// Select drpdown = new Select(driver.findElement(By.xpath("//div[text() = 'My
		// Account']")));
		//
		// drpdown.selectByVisibleText("Logout");

		// click the X button

		// driver.findElement(By.xpath("//button[text()='✕']")).click();
		
		Thread.sleep(1000);

		// search product

//		 driver.findElement(By.xpath("//input[@placeholder = 'Search for products, brands and more']")).sendKeys("iphone 11");

		 //Search product using 'contains'
		 
		 driver.findElement(By.xpath("//input[contains(@placeholder,'Search')]")).sendKeys("iphone 11");
		 
		// search product

		 driver.findElement(By.xpath("//button[@type = 'submit']")).click();

		// click on top offer
		// driver.findElement(By.xpath("//div[text()='Top Offers']")).click();

		// put mobile number

		// driver.findElement(By.xpath("//div[@class='IiD88i
		// _351hSN']")).sendKeys("7865");

		// Thread.sleep(200);
		//
		// //click continue
		//
		// driver.findElement(By.xpath("(//button[@type = 'submit'])[2]")).click();

		// driver.findElement(By.linkText("English (UK)")).click();
		// driver.findElement(By.linkText("বাংলা")).click();
		// driver.findElement(By.linkText("Home")).click();

	}

}

// driver.get("https://www.javatpoint.com/java-tutorial");

// driver.get("https://www.facebook.com/");

// driver.findElement(By.linkText("Login")).click();