package selenium.training;

import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Popup {

	public static void main(String[] args) {
		
		System.setProperty("webdriver.chrome.driver", "D:\\Automation testing\\chromedriver\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		driver.get("http://www.rediff.com/");
		driver.manage().window().maximize();
		String parentWindowHandler = driver.getWindowHandle(); // Store your parent window

		
		driver.findElement(By.xpath("//a[contains(@title,'Sign')]")).click();
		
		String subWindowHandler = null;

		Set<String> handles = driver.getWindowHandles(); // get all window handles
		Iterator<String> iterator = handles.iterator();
		while (iterator.hasNext()){
		    subWindowHandler = iterator.next();
		if(parentWindowHandler.equalsIgnoreCase(subWindowHandler)) {
		driver.switchTo().window(subWindowHandler); // switch to pop up window

		// Now we are in the pop up window, perform necessary actions here
		
		driver.findElement(By.xpath("//input[@id='login1']")).sendKeys("hi");
		}
		driver.switchTo().window(parentWindowHandler);  // switch back to parent window

	}


	}

}
