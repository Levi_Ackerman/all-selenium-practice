package selenium.training;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Windowhandle3 {

	public static void main(String[] args) {
		
		System.setProperty("webdriver.chrome.driver", "D:\\\\Automation testing\\\\chromedriver\\\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		driver.get("https://www.naukri.com/");
		
		driver.findElement(By.xpath("//div[contains(text(),'Recruiters')]")).click();
		
		driver.findElement(By.xpath("//div[contains(text(),'Companies')]")).click();

		
		String parentwindow = driver.getWindowHandle();
		System.out.println(" Parent window : " + parentwindow);
		
		Set<String> allwindows = driver.getWindowHandles();
		System.out.println("Windows: "+ allwindows);
		
		int Numberofallwindows = allwindows.size();
		System.out.println("number of windows: "+ Numberofallwindows);
		
//		String tb = Keys.chord(Keys.CONTROL,Keys.ENTER);
//		driver.findElement(By.xpath("//*contains(text(),'Hr')")).sendKeys(tb);
		
		
		
		for(String child : allwindows) {
			
			if(!parentwindow.equalsIgnoreCase(child)) {
				
				driver.switchTo().window(child);
				driver.manage().timeouts().implicitlyWait(2000, TimeUnit.MILLISECONDS);

//				driver.findElement(By.xpath("//div[contains(text(),'Companies')]")).click();
				
				System.out.println(driver.getTitle());
			}
			
			driver.switchTo().window(parentwindow);
			
		}
		
		driver.manage().timeouts().implicitlyWait(2000, TimeUnit.MILLISECONDS);
		Set<String> allwindows1 = driver.getWindowHandles();
		System.out.println("Windowa: "+allwindows1);

		int window = allwindows1.size();
		System.out.println("Number of window: "+ window);

	}

}
