package selenium.training;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class TableRowCount {

	public static void main(String[] args) {
		
		
		System.setProperty("webdriver.chrome.driver", "D:\\Automation testing\\chromedriver\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		driver.get("https://studyeasy.org/java/50-keywords-of-java/");
		driver.manage().window().maximize();
		
		
		List<WebElement>  rows = driver.findElements(By.xpath("//table//following::tr")); 
        System.out.println("No of rows are : " + rows.size());
        
        List<WebElement> coloumns = driver.findElements(By.xpath("(//table//following::tr)[1]//following-sibling::td"));
        System.out.println("No of coloumns are : " + coloumns.size());
        
        List<WebElement>  data = driver.findElements(By.xpath("//table//following::td")); 
        System.out.println("No of datas are : " + data.size());

	}

}
