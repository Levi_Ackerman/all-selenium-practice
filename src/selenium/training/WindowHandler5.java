package selenium.training;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class WindowHandler5 {

	public static void main(String[] args) {
		
		System.setProperty("webdriver.chrome.driver", "D:\\\\Automation testing\\\\chromedriver\\\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		driver.get("https://google.com");
		driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL + "t");
		
		System.out.println(driver.getTitle());
		
		driver.get("https://bing.com");
		System.out.println(driver.getTitle());
		
		
		driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL + "\t");
		
		System.out.println(driver.getTitle());

	}

}
