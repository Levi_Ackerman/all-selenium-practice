package selenium.training;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class DynamicTableHandling1 {

	public static void main(String[] args) {
		
		System.setProperty("webdriver.chrome.driver", "D:\\Automation testing\\chromedriver\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		driver.get("https://chercher.tech/practice/dynamic-table");
		driver.manage().window().maximize();
		
		
		driver.findElement(By.xpath("//td[contains(text(),'google')]//preceding-sibling::td//input[@type='checkbox']")).click();
		driver.findElement(By.xpath("//td[contains(text(),'facebook.com')]//preceding-sibling::td//input[@type='checkbox']")).click();
				
	}

}
