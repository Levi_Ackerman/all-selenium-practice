package selenium.training;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class ChromeLunchDemo {

	public static void main(String[] args) {

		// Setting the webdriver.chrome.driver property to its executable's location
		System.setProperty("webdriver.chrome.driver", "D:\\Automation testing\\chromedriver\\chromedriver.exe");

		// Instantiating driver object
		WebDriver driver = new ChromeDriver();

		// Using get() method to open a web page
		driver.get("https://www.lambdatest.com/");

	}

}
